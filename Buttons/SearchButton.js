"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _search = require("material-ui/svg-icons/action/search");

var _search2 = _interopRequireDefault(_search);

var _RaisedButton = require("material-ui/RaisedButton");

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var SearchButton = function SearchButton(props) {
    var label = props.label,
        onTouchTap = props.onTouchTap;

    return _react2.default.createElement(_RaisedButton2.default, {
        label: label,
        secondary: true,
        icon: _react2.default.createElement(_search2.default, null),
        onTouchTap: onTouchTap
    });
};

SearchButton.propTypes = {
    label: _propTypes2.default.string.isRequired,
    onTouchTap: _propTypes2.default.func
};

exports.default = SearchButton;

import path from "path";
import ExtractTextPlugin from "extract-text-webpack-plugin";


let wp = {};

const context = path.resolve(__dirname, "./src"),
    devtool = "cheap-module-source-map",
    entry = {
        app: "./app.jsx",
        vendor: ["react"]
    },
    extensions = [".js", ".es6", ".json", ".jsx", ".scss"],
    output = {
        path: path.resolve(__dirname, "./dist"),
        publicPath: "/",
        filename: "./[name].js"
    },
    plugins = [
        new ExtractTextPlugin({
            filename: "[name].css?v=[contenthash]",
            allChunks: true
        })
    ],
    rules = [
        {
            test: /\.(es6|jsx)$/,
            exclude: /node_modules/,
            use: "babel-loader"
        },
        {
            test: /\.(jpg|jpeg|png|gif)$/,
            exclude: /node_modules/,
            use: [
                {
                    loader: "url-loader",
                    options: { limit: 10000 }
                }
            ]
        },
        {
            test: /\.(eot|ttf|woff|woff2|svg|otf)$/,
            use: [{ loader: "file-loader" }]
        },

        {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: [
                    {
                        loader: "css-loader",
                        options: { modules: false, sourceMaps: true }
                    }
                ]
            }),
            include: /flexboxgrid/
        }
    ];

wp.context = context;
wp.entry = entry;
wp.output = output;
wp.devtool = devtool;
wp.module = {
    rules: rules
};
wp.resolve = {
    alias:  {
        "financiera-ui": path.resolve(__dirname, "./src/components/")
    },
    extensions: extensions
};
wp.plugins = plugins;
wp.watch = false;


export default wp;

"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Dialog = require("material-ui/Dialog");

var _Dialog2 = _interopRequireDefault(_Dialog);

var _FlatButton = require("material-ui/FlatButton");

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _Spinner = require("../Spinner/Spinner");

var _Spinner2 = _interopRequireDefault(_Spinner);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @param {{}} props : dfnodsfnkl
 * @return {XML} : knoofdnkl
 * @constructor
 */
var Modal = function Modal(props) {
    var title = props.title,
        open = props.open,
        _props$enableEsc = props.enableEsc,
        enableEsc = _props$enableEsc === undefined ? true : _props$enableEsc,
        _props$showSpinner = props.showSpinner,
        showSpinner =
            _props$showSpinner === undefined ? false : _props$showSpinner,
        _props$disabledOk = props.disabledOk,
        disabledOk =
            _props$disabledOk === undefined ? false : _props$disabledOk,
        _props$labelOk = props.labelOk,
        labelOk = _props$labelOk === undefined ? "Aceptar" : _props$labelOk,
        _props$labelCancel = props.labelCancel,
        labelCancel =
            _props$labelCancel === undefined ? "Cancelar" : _props$labelCancel,
        onRequestClose = props.onRequestClose,
        onTouchTapOk = props.onTouchTapOk,
        onTouchTapCancel = props.onTouchTapCancel,
        _props$contentStyle = props.contentStyle,
        contentStyle =
            _props$contentStyle === undefined
                ? {
                      width: "95%",
                      maxWidth: "none"
                  }
                : _props$contentStyle,
        actions = [];

    if (onTouchTapCancel) {
        actions.push(
            onTouchTapCancel &&
                _react2.default.createElement(_FlatButton2.default, {
                    label: labelCancel,
                    primary: true,
                    onTouchTap: onTouchTapCancel
                })
        );
    }

    if (onTouchTapOk) {
        actions.push(
            onTouchTapOk &&
                _react2.default.createElement(_FlatButton2.default, {
                    label: labelOk,
                    disabled: disabledOk,
                    primary: true,
                    onTouchTap: onTouchTapOk
                })
        );
    }

    return _react2.default.createElement(
        _Dialog2.default,
        {
            title: title,
            actions: actions,
            modal: enableEsc,
            autoScrollBodyContent: true,
            contentStyle: contentStyle,
            open: open,
            onRequestClose: onRequestClose
        },
        props.children,
        showSpinner &&
            _react2.default.createElement(_Spinner2.default, {
                visible: showSpinner
            })
    );
};

Modal.propTypes = {
    title: _propTypes2.default.string.isRequired,
    labelOk: _propTypes2.default.string,
    labelCancel: _propTypes2.default.string,
    open: _propTypes2.default.bool.isRequired,
    onRequestClose: _propTypes2.default.func,
    onTouchTapOk: _propTypes2.default.func,
    onTouchTapCancel: _propTypes2.default.func,
    showSpinner: _propTypes2.default.bool,
    disabledOk: _propTypes2.default.bool,
    enableEsc: _propTypes2.default.bool,
    contentStyle: _propTypes2.default.object,
    children: _propTypes2.default.node
};

exports.default = Modal;

"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _delete = require("material-ui/svg-icons/action/delete");

var _delete2 = _interopRequireDefault(_delete);

var _IconButton = require("material-ui/IconButton");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Avatar = require("material-ui/Avatar");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _assignment = require("material-ui/svg-icons/action/assignment");

var _assignment2 = _interopRequireDefault(_assignment);

var _colors = require("material-ui/styles/colors");

var _List = require("material-ui/List");

var _FilesManagerUtil = require("./FilesManagerUtil");

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var FileItem = function FileItem(props) {
    var name = props.name,
        type = props.type,
        size = props.size,
        onRemove = props.onRemove;

    return _react2.default.createElement(_List.ListItem, {
        leftAvatar: _react2.default.createElement(_Avatar2.default, {
            icon: _react2.default.createElement(_assignment2.default, null),
            backgroundColor: _colors.blue500
        }),
        rightIconButton: _react2.default.createElement(
            _IconButton2.default,
            { onTouchTap: onRemove },
            _react2.default.createElement(_delete2.default, null)
        ),
        primaryText: name,
        secondaryText: type + " " + (0, _FilesManagerUtil.formatBytes)(size)
    });
};

FileItem.propTypes = {
    name: _propTypes2.default.string.isRequired,
    onRemove: _propTypes2.default.func.isRequired,
    type: _propTypes2.default.string,
    size: _propTypes2.default.number
};

exports.default = FileItem;

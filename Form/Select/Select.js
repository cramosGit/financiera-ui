"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _SelectField = require("material-ui/SelectField");

var _SelectField2 = _interopRequireDefault(_SelectField);

var _TextField = require("material-ui/TextField");

var _TextField2 = _interopRequireDefault(_TextField);

var _MenuItem = require("material-ui/MenuItem");

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _AutoComplete = require("material-ui/AutoComplete");

var _AutoComplete2 = _interopRequireDefault(_AutoComplete);

var _SelectUtil = require("./SelectUtil");

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var Select = function Select(props) {
    var label = props.label,
        disabled = props.disabled,
        disabledOption = props.disabledOption,
        name = props.name,
        errorText = props.errorText,
        multiple = props.multiple,
        collection = props.collection,
        required = props.required,
        autoComplete = props.autoComplete,
        maxHeight = props.maxHeight,
        floatingLabelFixed = props.floatingLabelFixed,
        onChange = props.onChange;
    var value = props.value;

    var onChangeSelectField = function onChangeSelectField(
        event,
        index,
        newValue
    ) {
        var selected = void 0;

        if (multiple) {
            selected = [];
            newValue.map(function(nv) {
                if (nv !== null) {
                    selected.push(nv);
                }
            });
        } else {
            selected = collection[index - 1];
        }
        onChange(name, selected);
    };

    if (disabled) {
        return _react2.default.createElement(_TextField2.default, {
            hintText: label,
            disabled: disabled,
            floatingLabelFixed: floatingLabelFixed,
            floatingLabelText: label,
            name: name,
            fullWidth: true
        });
    }

    if (!autoComplete) {
        return _react2.default.createElement(
            _SelectField2.default,
            {
                floatingLabelText: label,
                value: (0, _SelectUtil.getSelectedValue)(multiple, value),
                errorText: (0, _SelectUtil.getTextError)({
                    multiple: multiple,
                    required: required,
                    value: value,
                    errorText: errorText
                }),
                maxHeight: maxHeight,
                disabled: disabledOption,
                fullWidth: true,
                multiple: multiple,
                onChange: onChangeSelectField
            },
            _react2.default.createElement(_MenuItem2.default, {
                value: (0, _SelectUtil.getDefaultValue)(multiple),
                primaryText: "Selecciona una opci\xF3n"
            }),
            collection.map(function(item, i) {
                return _react2.default.createElement(_MenuItem2.default, {
                    key: "form-select-" + name + "-" + i + "-item",
                    checked: (0, _SelectUtil.isCheckedValue)(
                        value,
                        multiple,
                        item
                    ),
                    value: (0, _SelectUtil.getItemValue)(multiple, item),
                    primaryText: item.text
                });
            })
        );
    }

    return _react2.default.createElement(_AutoComplete2.default, {
        name: name,
        searchText: value.text,
        floatingLabelText: label,
        filter: _AutoComplete2.default.fuzzyFilter,
        dataSource: collection,
        fullWidth: true,
        menuStyle: { maxHeight: maxHeight, overflowY: "auto" },
        errorText: (0, _SelectUtil.getTextError)({
            multiple: multiple,
            required: required,
            value: value,
            errorText: errorText,
            autoComplete: autoComplete
        }),
        openOnFocus: true,
        popoverProps: {
            style: {
                overflowY: "auto"
            }
        },
        onUpdateInput: function onUpdateInput(searchText) {
            if (searchText.length > 0) {
                return;
            }
            onChange(name, { text: "" });
        },
        onNewRequest: function onNewRequest(selected) {
            onChange(name, selected);
        }
    });
};

Select.propTypes = {
    onChange: _propTypes2.default.func.isRequired
};

Select.defaultProps = {
    value: {},
    collection: [],
    autoComplete: true,
    disabled: false,
    multiple: false,
    required: true,
    errorText: "Selecciona una opción *"
};

exports.default = Select;

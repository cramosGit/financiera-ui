"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

// TODO, temporal
var getTextError = (exports.getTextError = function getTextError(data) {
    var required = data.required,
        value = data.value,
        errorText = data.errorText,
        multiple = data.multiple,
        autoComplete = data.autoComplete;

    if (required) {
        if (!autoComplete && multiple) {
            if (!Array.isArray(value)) {
                return errorText;
            }

            if (value.length === 0) {
                return errorText;
            }

            return;
        }

        if (
            typeof value === "undefined" ||
            value === null ||
            typeof value.value === "undefined" ||
            value.value === null ||
            value.value === ""
        ) {
            return errorText;
        }
    }
});

var isCheckedValue = (exports.isCheckedValue = function isCheckedValue(
    value,
    multiple,
    item
) {
    if (multiple) {
        if (!Array.isArray(value)) {
            value = [];
        }
        return (
            value.findIndex(function(a) {
                return item.value === a.value;
            }) !== -1
        );
    } else {
        return item.value === value.value;
    }
});

var getDefaultValue = (exports.getDefaultValue = function getDefaultValue(
    multiple
) {
    if (multiple) {
        return null;
    } else {
        return {};
    }
});

var getSelectedValue = (exports.getSelectedValue = function getSelectedValue(
    multiple,
    value
) {
    if (multiple) {
        if (!Array.isArray(value)) {
            return [];
        }
        return value;
    } else {
        return value.value;
    }
});

var getItemValue = (exports.getItemValue = function getItemValue(
    multiple,
    item
) {
    if (multiple) {
        return item;
    } else {
        return item.value;
    }
});

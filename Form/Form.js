"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = require("babel-runtime/helpers/extends");

var _extends3 = _interopRequireDefault(_extends2);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

var _TextField = require("material-ui/TextField");

var _TextField2 = _interopRequireDefault(_TextField);

var _fileUpload = require("material-ui/svg-icons/file/file-upload");

var _fileUpload2 = _interopRequireDefault(_fileUpload);

var _FloatingActionButton = require("material-ui/FloatingActionButton");

var _FloatingActionButton2 = _interopRequireDefault(_FloatingActionButton);

var _Checkbox = require("material-ui/Checkbox");

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _RadioButton = require("material-ui/RadioButton");

var _DatePicker = require("material-ui/DatePicker");

var _DatePicker2 = _interopRequireDefault(_DatePicker);

var _intlLocalesSupported = require("intl-locales-supported");

var _intlLocalesSupported2 = _interopRequireDefault(_intlLocalesSupported);

var _formats = require("../util/formats");

var _emitter = require("../util/emitter");

var _events = require("../util/events");

var _events2 = _interopRequireDefault(_events);

var _FormUtil = require("./FormUtil");

var _Select = require("./Select/Select");

var _Select2 = _interopRequireDefault(_Select);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var DateTimeFormat = void 0,
    onChangeValueEvent = function onChangeValueEvent(data) {
        var value = data.value,
            name = data.name,
            form = data.form,
            epic = data.epic,
            module = data.module;

        _emitter.EE.emit(_events2.default.FORM_CHANGE_VALUE, {
            value: value,
            name: name,
            form: form,
            epic: epic,
            module: module
        });
    };

if ((0, _intlLocalesSupported2.default)(["es", "es-MX"])) {
    DateTimeFormat = global.Intl.DateTimeFormat;
}

var Form = function Form(props) {
    var styleContainer = props.styleContainer,
        inputs = props.inputs,
        onChangeInputs = props.onChangeInputs,
        onKeyPress = props.onKeyPress,
        onFocus = props.onFocus,
        onBlur = props.onBlur,
        formName = props.formName,
        epic = props.epic,
        module = props.module,
        maxHeight = props.maxHeight,
        underlineStyle = props.underlineStyle,
        underlineFocusStyle = props.underlineFocusStyle,
        handleOnChange = function handleOnChange(name, value) {
            if (typeof onChangeInputs === "undefined") {
                onChangeValueEvent({
                    value: value,
                    name: name,
                    form: formName,
                    epic: epic,
                    module: module
                });
            } else {
                onChangeInputs(value, name);
            }
        },
        getForm = function getForm() {
            return (
                inputs &&
                inputs.length &&
                inputs.map(function(property, i) {
                    var _property$value = property.value,
                        value =
                            _property$value === undefined
                                ? ""
                                : _property$value,
                        _property$options = property.options,
                        options =
                            _property$options === undefined
                                ? []
                                : _property$options,
                        _property$type = property.type,
                        type =
                            _property$type === undefined
                                ? "textfield"
                                : _property$type,
                        label = property.label,
                        mini = property.mini,
                        _property$multiLine = property.multiLine,
                        multiLine =
                            _property$multiLine === undefined
                                ? false
                                : _property$multiLine,
                        rows = property.rows,
                        unix = property.unix,
                        _property$disabled = property.disabled,
                        disabled =
                            _property$disabled === undefined
                                ? false
                                : _property$disabled,
                        _property$hidden = property.hidden,
                        hidden =
                            _property$hidden === undefined
                                ? false
                                : _property$hidden,
                        _property$multiple = property.multiple,
                        multiple =
                            _property$multiple === undefined
                                ? false
                                : _property$multiple,
                        _property$accept = property.accept,
                        accept =
                            _property$accept === undefined
                                ? []
                                : _property$accept,
                        min = property.min,
                        max = property.max,
                        name = property.name,
                        autoOk = property.autoOk,
                        errorText = (0, _FormUtil.getTextError)(property),
                        inputId = name;

                    if (hidden) {
                        return;
                    }

                    switch (type) {
                        case "checkbox":
                            if (typeof value !== "boolean") {
                                value = false;
                            }

                            return _react2.default.createElement(
                                _Checkbox2.default,
                                {
                                    key: "form-checkbox-" + i,
                                    label: label,
                                    name: name,
                                    onCheck: function onCheck(
                                        event,
                                        isInputChecked
                                    ) {
                                        handleOnChange(inputId, isInputChecked);
                                    },
                                    style: { marginTop: "20px" },
                                    checked: value,
                                    labelPosition: "left"
                                }
                            );

                        case "select":
                            return _react2.default.createElement(
                                _Select2.default,
                                (0, _extends3.default)(
                                    {
                                        maxHeight: maxHeight,
                                        key: "form-select-" + name + "-" + i
                                    },
                                    property,
                                    {
                                        onChange: handleOnChange
                                    }
                                )
                            );
                        case "date":
                            value = (0, _formats.getDateObject)(value, unix);

                            var maxDate = null,
                                minDate = null;

                            if (min) {
                                minDate = (0, _moment2.default)(
                                    min,
                                    "DD/MM/YYYY"
                                ).toDate();
                            }

                            if (max) {
                                maxDate = (0, _moment2.default)(
                                    max,
                                    "DD/MM/YYYY"
                                ).toDate();
                            }

                            return _react2.default.createElement(
                                _DatePicker2.default,
                                {
                                    key: "form-date-picker-" + i,
                                    hintText: label,
                                    value: value,
                                    minDate: minDate,
                                    maxDate: maxDate,
                                    floatingLabelText: label,
                                    fullWidth: true,
                                    disabled: disabled,
                                    locale: "es-MX",
                                    errorText: errorText,
                                    DateTimeFormat: DateTimeFormat,
                                    container: "inline",
                                    mode: "landscape",
                                    autoOk: autoOk,
                                    onChange: function onChange(
                                        event,
                                        newDate
                                    ) {
                                        handleOnChange(inputId, newDate);
                                    }
                                }
                            );

                        case "file":
                            return _react2.default.createElement(
                                _FloatingActionButton2.default,
                                {
                                    key: "form-file-" + i,
                                    containerElement: "label",
                                    secondary: true,
                                    mini: mini,
                                    disabled: disabled,
                                    className: "mn-float-a-btn",
                                    label: label
                                },
                                _react2.default.createElement(
                                    _fileUpload2.default,
                                    null
                                ),
                                !disabled &&
                                    _react2.default.createElement("input", {
                                        multiple: multiple,
                                        accept: accept.join(),
                                        type: "file",
                                        style: { display: "none" },
                                        onClick: function onClick(e) {
                                            e.target.value = null;
                                        },
                                        onChange: function onChange(e) {
                                            var formData = new FormData();
                                            formData.append(
                                                name,
                                                e.target.files[0]
                                            );
                                            handleOnChange(inputId, formData);
                                        }
                                    })
                            );

                        case "radioButton":
                            return _react2.default.createElement(
                                _RadioButton.RadioButtonGroup,
                                {
                                    key: "form-radio-group-" + i,
                                    name: "shipSpeed",
                                    style: { display: "flex" },
                                    onChange: function onChange(
                                        event,
                                        selected
                                    ) {
                                        handleOnChange(inputId, selected);
                                    },
                                    valueSelected: value
                                },
                                options.map(function(option, index) {
                                    return _react2.default.createElement(
                                        _RadioButton.RadioButton,
                                        {
                                            key: "form-radio-item-" + index,
                                            value: option.value,
                                            style: {
                                                display: "inline-block",
                                                minWidth: "210px"
                                            },
                                            disabled: option.disabled,
                                            label: option.label
                                        }
                                    );
                                })
                            );

                        case "textfield":
                        default:
                            return _react2.default.createElement(
                                _TextField2.default,
                                {
                                    key: "form-textfield-" + i,
                                    hintText: label,
                                    errorText: errorText,
                                    disabled: disabled && disabled,
                                    floatingLabelText: label,
                                    value:
                                        typeof value !== "undefined" && value,
                                    rows: rows,
                                    multiLine: multiLine,
                                    onChange: function onChange(
                                        event,
                                        newValue
                                    ) {
                                        handleOnChange(inputId, newValue);
                                    },
                                    onKeyPress: onKeyPress,
                                    onFocus: onFocus,
                                    onBlur: onBlur,
                                    name: name,
                                    fullWidth: true,
                                    underlineStyle: underlineStyle,
                                    underlineFocusStyle: underlineFocusStyle
                                }
                            );
                    }
                })
            );
        };

    return _react2.default.createElement(
        "div",
        { style: styleContainer },
        getForm()
    );
};

Form.propTypes = {
    onChangeInputs: _propTypes2.default.func,
    onKeyPress: _propTypes2.default.func,
    onFocus: _propTypes2.default.func,
    onBlur: _propTypes2.default.func,
    formName: _propTypes2.default.string,
    epic: _propTypes2.default.string,
    module: _propTypes2.default.string,
    styleContainer: _propTypes2.default.object,
    inputs: _propTypes2.default.array.isRequired,
    customError: _propTypes2.default.string,
    passedValidation: _propTypes2.default.bool,
    maxHeight: _propTypes2.default.number
};

exports.default = Form;

import React from "react";
import PropTypes from "prop-types";

const Props = ({ props }) => {
    return (
        <table className="props">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Tipo</th>
                    <th>Default</th>
                    <th>Requerido</th>
                    <th>Descripción</th>
                </tr>
            </thead>
            <tbody>
                {Object.keys(props).map(key => {
                    return (
                        <tr className="text-left" key={key}>
                            <td>{key}</td>
                            <td>{props[key].type.name}</td>
                            <td>
                                {props[key].defaultValue &&
                                    props[key].defaultValue.value}
                            </td>
                            <td>{props[key].required && "X"}</td>
                            <td
                                dangerouslySetInnerHTML={{
                                    __html: props[key].description
                                }}
                            />
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
};

Props.propTypes = {
    props: PropTypes.object.isRequired
};

export default Props;

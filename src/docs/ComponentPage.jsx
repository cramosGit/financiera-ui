import React from "react";
import PropTypes from "prop-types";
import Example from "./Example";
import Props from "./Props";

const ComponentPage = ({ component }) => {
    const { name, description, props, examples = [] } = component;

    return (
        <div className="componentpage">
            <h2>{name}</h2>
            <p>{description}</p>

            <h3>Ejemplo {examples.length > 1 && "s"}</h3>
            {examples.length > 0
                ? examples.map(example => (
                      <Example
                          key={example.code}
                          example={example}
                          componentName={name}
                      />
                  ))
                : "No existe ejemplo de uso. Agregar"}

            <h3>Propiedades del componente</h3>
            {props ? (
                <Props props={props} />
            ) : (
                "Este componente no acepta propiedades."
            )}
        </div>
    );
};

ComponentPage.propTypes = {
    component: PropTypes.object.isRequired
};

export default ComponentPage;

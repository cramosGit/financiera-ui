// @flow
import React, { Component } from "react";
import { DataTable } from "financiera-ui";

export default class App extends Component {
    constructor(props: {}, context: {}) {
        super(props, context);
        this.headers = [
            {
                key: "name",
                label: "Nombre"
            },
            {
                key: "lastName",
                label: "Apellido"
            },
            {
                key: "username",
                label: "Nombre de usuario"
            },
            {
                key: "money",
                label: "Dinero",
                type: "currency"
            },
            {
                key: "date",
                label: "Fecha",
                type: "date"
            }
        ];
        this.data = [
            {
                name: "Juan",
                lastName: "Crisóstomo",
                username: "darcusfenix",
                money: 750,
                date: 703555200000
            }
        ];
    }

    render = () => {
        return (
            <DataTable
                title="Lista de usuarios"
                headers={this.headers}
                data={this.data}
            />
        );
    };
}

// @flow
import React, { Component } from "react";
import { BreadCrumbs } from "financiera-ui";

export default class App extends Component {
    constructor(props: {}, context: {}) {
        super(props, context);
        this.handleClick = ::this.handleClick;
        this.items = [
            {
                primaryText: "Pagina 1",
                url: "/pagina-1"
            },
            {
                primaryText: "Pagina 2",
                url: "/pagina-2"
            }
        ];
    }

    handleClick = (item: {}) => {
        alert(item.toString());
    };

    render = () => {
        return (
            <div
                style={{
                    width: "100%",
                    height: "75px",
                    backgroundColor: "blue"
                }}
            >
                <BreadCrumbs items={this.items} onTouchTap={this.handleClick} />
            </div>
        );
    };
}

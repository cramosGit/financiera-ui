import React, { Component } from "react";
import Navigation from "./Navigation";
import logo from "../assets/images/logo.gif";
import ComponentPage from "./ComponentPage";
import componentData from "../../docs/componentData";

export default class Docs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            route: window.location.hash.substr(1)
        };
    }

    componentDidMount = () => {
        window.addEventListener("hashchange", () => {
            this.setState({ route: window.location.hash.substr(1) });
        });
    };

    render = () => {
        const { route } = this.state;
        const component = route
            ? componentData.filter(component => component.name === route)[0]
            : componentData[0];

        return (
            <div>
                <div>
                    <div className="logo-container">
                        <img src={logo} />
                    </div>
                    <div className="title-container">
                        <h1>Financiera-UI</h1>
                        <p>
                            Librería de componentes personalizados con base en
                            Material-UI para la integración y proyecto Más
                            Nómina.
                        </p>
                    </div>
                </div>
                <Navigation
                    components={componentData.map(component => component.name)}
                />
                <ComponentPage component={component} />
            </div>
        );
    };
}

import React from "react";
import PropTypes from "prop-types";

import DeleteInfo from "material-ui/svg-icons/action/delete";
import IconButton from "material-ui/IconButton";
import Avatar from "material-ui/Avatar";
import ActionAssignment from "material-ui/svg-icons/action/assignment";
import { blue500 } from "material-ui/styles/colors";
import { ListItem } from "material-ui/List";
import { formatBytes } from "./FilesManagerUtil";

const FileItem = props => {
    const { name, type, size, onRemove } = props;
    return (
        <ListItem
            leftAvatar={
                <Avatar icon={<ActionAssignment />} backgroundColor={blue500} />
            }
            rightIconButton={
                <IconButton onTouchTap={onRemove}>
                    <DeleteInfo />
                </IconButton>
            }
            primaryText={name}
            secondaryText={`${type} ${formatBytes(size)}`}
        />
    );
};

FileItem.propTypes = {
    name: PropTypes.string.isRequired,
    onRemove: PropTypes.func.isRequired,
    type: PropTypes.string,
    size: PropTypes.number
};

export default FileItem;

// @flow
import React, { Component } from "react";
import PropTypes from "prop-types";
import Dialog from "material-ui/Dialog";
import RaisedButton from "material-ui/RaisedButton";
import FileUploadIcon from "material-ui/svg-icons/file/file-upload";
import { List } from "material-ui/List";
import FileItem from "./FileItem";

export default class FileManager extends Component {
    static propTypes = {
        open: PropTypes.bool,
        onChange: PropTypes.func,
        onClose: PropTypes.func.isRequired
    };

    static defaultProps = {
        open: false
    };

    state: { files: [] };

    constructor(props: {}, context: {}) {
        super(props, context);
        this.handleOnChangeInput = ::this.handleOnChangeInput;
        this.handleClose = ::this.handleClose;

        this.state = {
            files: []
        };
    }

    componentDidMount = () => {};

    componentWillUpdate = (nextProps: {}, nextState) => {
        const { files } = nextState,
            { open } = nextProps;

        if (files.length !== this.state.files.length) {
            if (open && typeof this.props.onChange !== "undefined") {
                this.props.onChange(files);
            }
        }
    };

    componentWillUnmount = () => {};

    handleClose = () => {
        this.props.onClose(this.state.files);
        this.setState({ files: [] });
    };

    handleOnChangeInput = e => {
        let { files } = this.state;
        for (let i = 0; i < e.target.files.length; i += 1) {
            files = [e.target.files[i], ...files];
        }
        this.setState({ files });
    };

    handleRemoveIcon = (index: number) => {
        let files = [...this.state.files];
        files.splice(index, 1);

        this.setState({ files });
    };

    render = () => {
        const { files } = this.state,
            { open } = this.props;

        const actions = [
            <RaisedButton
                label="Seleccionar archivos de tu ordenador"
                containerElement="label"
                secondary={true}
                style={{ marginRight: "10px" }}
                icon={<FileUploadIcon />}
            >
                <input
                    multiple={true}
                    type="file"
                    style={{ display: "none" }}
                    onClick={e => {
                        e.target.value = null;
                    }}
                    onChange={this.handleOnChangeInput}
                />
            </RaisedButton>,
            <RaisedButton
                label="Aceptar"
                disabled={files.length === 0}
                secondary={true}
                onTouchTap={this.handleClose}
            />
        ];

        return (
            <Dialog
                title={`Adjuntar ${
                    files.length > 0 ? files.length : ""
                } archivos`}
                actions={actions}
                modal={false}
                autoScrollBodyContent={true}
                contentStyle={{ width: "700px" }}
                open={open}
                onRequestClose={this.handleClose}
            >
                <List>
                    {files.map((file, index) => (
                        <FileItem
                            key={`list-item-file-${index}`}
                            name={file.name}
                            type={file.type}
                            size={file.size}
                            onRemove={() => {
                                this.handleRemoveIcon(index);
                            }}
                        />
                    ))}
                </List>
            </Dialog>
        );
    };
}

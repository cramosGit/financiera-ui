// @flow
import Request from "../util/Request";

export default class AutorizacionApi extends Request {
    constructor() {
        super();
    }

    validar = async (email: string, baseUrl: string) => {
        try {
            let user = {};
            let response = await this.fetch({
                baseUrl,
                url: "cxf/seguridad/autorizacion/consultarUsuarioEmail",
                data: {
                    email
                }
            });

            user.idPersona = response.idPersona;
            user.claveUsuario = response.claveUsuario;
            user.email = response.email;

            if (response === null) {
                throw new Error("El correo no es válido");
            }
            response = await this.fetch({
                baseUrl,
                url: "cxf/empleados/rest/buscarActivos",
                data: {
                    usuario: user.claveUsuario
                }
            });

            user.nombre = response.nombre;
            user.apellidoPaterno = response.apellidoPaterno;
            user.apellidoMaterno = response.apellidoMaterno;
            user.sucursal = {};
            user.sucursal.clave = response.claveSucursal;
            user.sucursal.descripcion = response.descripcionSucursal;
            user.puesto = {};
            user.puesto.clave = response.clavePuesto;
            user.puesto.descripcion = response.descripcionPuesto;
            user.empresa = {};
            user.empresa.clave = response.claveEmpresa;
            user.empresa.descripcion = response.claveEmpresa;

            if (response.length === 0) {
                throw new Error("El usuario no está activo");
            }
            response = await this.fetch({
                baseUrl,
                url: "cxf/seguridad/autorizacion/consultarUsuarios",
                data: {
                    claveUsuario: user.claveUsuario,
                    facultades: [{}],
                    perfiles: [{}]
                }
            });

            if (response.length === 0) {
                throw new Error(
                    "No hay facultades y perfiles para este usuario"
                );
            }

            return {
                ...user,
                perfiles: response[0].perfiles,
                facultades: response[0].facultades
            };
        } catch (err) {
            throw new Error(err);
        }
    };
}

// @flow

import axios from "axios";
import AutorizacionApi from "../AutorizacionApi";
import config from "../../config";

export const isThereATokenValid = async (
        data: { email?: string, isSanbox: true } = { isSanbox: true }
    ) => {
        const { email, isSanbox } = data;
        try {
            let response,
                token = sessionStorage.getItem("token"),
                userValid;

            if (typeof token === "undefined" || token === null) {
                return false;
            }
            const instance = axios.create();
            response = await instance.request({
                url: "/oauth2/v3/tokeninfo",
                method: "get",
                baseURL: "https://www.googleapis.com/",
                params: {
                    access_token: token
                },
                timeout: 100000
            });
            userValid = response.data["email_verified"] === "true";
            response = await new AutorizacionApi().validar(
                email ? email : response.data.email,
                isSanbox
                    ? config.oauth.baseUrlDevelop
                    : config.oauth.baseUrlProduction
            );
            return {
                userValid,
                ...response
            };
        } catch (err) {
            return {
                userValid: false,
                perfiles: []
            };
        }
    },
    saveToken = (token: string) => {
        sessionStorage.setItem("token", token);
    };

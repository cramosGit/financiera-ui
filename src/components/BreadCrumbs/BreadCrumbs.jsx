// @flow
import React from "react";
import PropTypes from "prop-types";
import FlatButton from "material-ui/FlatButton";

/**
 * Pinta una lista vertical de FlatButton. Su objetivo es mostrar la navegación del
 * usuario en la spa.
 */
const BreadCrumbs = ({ items = [], onTouchTap }) => {
    return (
        <div className="row">
            {items.map((item, index) => {
                if (item === null) {
                    return;
                }

                const { primaryText, url } = item;
                let styleItem = { color: "rgba(255,255,255,0.7)" };

                if (index === items.length - 1) {
                    styleItem.color = "#FFFFFF";
                }

                return (
                    <div key={`bread-crumb-${index}`}>
                        >
                        <FlatButton
                            label={primaryText}
                            onTouchTap={() => {
                                onTouchTap(item);
                            }}
                            disabled={typeof url === "undefined"}
                            style={styleItem}
                        />
                    </div>
                );
            })}
        </div>
    );
};

BreadCrumbs.propTypes = {
    /** Callback ejecutado cuando el usuario da click sobre el botón. <br/><br/>
     *
     * function(item : {}) => void <br/>
     * @param item: Objeto seleccionado del array items. <br/>
     */
    onTouchTap: PropTypes.func.isRequired,
    /**
     * Array de objetos a pintar como listado
     * [
     *  {
     *      primaryText: string,
     *      url: string,
     *  }
     * ]
     */
    items: PropTypes.array.isRequired
};

export default BreadCrumbs;

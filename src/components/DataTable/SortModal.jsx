// @flow
import React from "react";
import PropTypes from "prop-types";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import { List, ListItem } from "material-ui/List";
import Divider from "material-ui/Divider";
import Checkbox from "material-ui/Checkbox";
import Toggle from "material-ui/Toggle";
import { getFormat } from "../util/formats";

const SortModal = (props: {
    open: boolean,
    sortName: string,
    sortType: string,
    onChangeSortType: Function,
    onClose: Function,
    onFilter: Function,
    distinct: {
        selected: [],
        collection: [],
        format: string
    },
    labelOrder: string,
    onCheckItem: Function
}): Node => {
    const {
        open,
        sortName,
        sortType,
        onChangeSortType,
        onClose,
        onFilter,
        distinct,
        labelOrder,
        onCheckItem
    } = props;

    const getOptionsGroup = () => {
        const options = distinct.columns[sortName];

        return options.collection.map((item, index) => {
            if (typeof item === "undefined" || item === null) {
                return;
            }

            let label = item;
            if (options.format !== "undefined" && options.format.length > 0) {
                label = getFormat({
                    type: options.format,
                    value: item
                });
            }
            let checked =
                options.selected.length > 0 &&
                options.selected.findIndex(i => i === item) > -1;

            return (
                <ListItem
                    key={`distinct-option-${index}`}
                    primaryText={label}
                    leftCheckbox={
                        <Checkbox
                            checked={checked}
                            onCheck={(event, value) => {
                                onCheckItem(item, value, sortName);
                            }}
                        />
                    }
                />
            );
        });
    };

    const actions = [
        <FlatButton label="Cancelar" onClick={onClose} />,
        <FlatButton label="Filtrar" onClick={onFilter} />
    ];

    return (
        <Dialog
            title={`Mostrar solo filas con valores de ${labelOrder}:`}
            actions={actions}
            modal={false}
            autoScrollBodyContent={true}
            contentStyle={{ width: "450px" }}
            open={open}
        >
            {open && (
                <List>
                    <ListItem
                        primaryText="¿Orden ascendente?"
                        rightToggle={
                            <Toggle
                                toggled={sortType === "asc"}
                                onToggle={onChangeSortType}
                            />
                        }
                    />
                    <Divider />
                    {getOptionsGroup(distinct)}
                </List>
            )}
        </Dialog>
    );
};

SortModal.propTypes = {
    labelOrder: PropTypes.string,
    sortType: PropTypes.string,
    open: PropTypes.bool,
    onChangeSortType: PropTypes.func,
    onCheckItem: PropTypes.func,
    onClose: PropTypes.func,
    onFilter: PropTypes.func,
    distinct: PropTypes.object
};

export default SortModal;

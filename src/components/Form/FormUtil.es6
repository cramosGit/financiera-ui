// @flow
export const getTextError = property => {
    const {
        value,
        errorText = "Campo requerido *",
        type = "textfield",
        pattern,
        required = true,
        customError,
        passedValidation
    } = property;

    if (
        (required && typeof value === "undefined") ||
        (required && value === "")
    ) {
        return errorText;
    }

    if (
        pattern &&
        type !== "select" &&
        type !== "checkbox" &&
        type !== "date" &&
        !pattern.test(value)
    ) {
        return errorText;
    }

    if (!passedValidation) {
        return customError;
    }

    return null;
};

// @flow
import React, { Component } from "react";
import PropTypes from "prop-types";
import { getDifferenceTime, getDuration } from "./elapsedTimeUtil";

export default class ElapsedTime extends Component {
    state: {
        years: number,
        months: number,
        days: number,
        hours: number,
        minutes: number,
        seconds: number
    };

    timer: number;

    static propTypes = {
        dateInitial: PropTypes.number,
        dateFinal: PropTypes.number,
        title: PropTypes.string,
        stop: PropTypes.bool,
        onlyDuration: PropTypes.bool,
        interval: PropTypes.number,
        addedTime: PropTypes.number
    };

    static defaultProps = {
        title: "TIEMPO TRANSCURRIDO",
        interval: 1000,
        addedTime: 0,
        stop: false,
        onlyDuration: false
    };

    constructor(props: {}, context: {}) {
        super(props, context);

        this.counting = ::this.counting;

        this.state = {
            years: 0,
            months: 0,
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        };
    }

    componentDidMount = () => {
        const { onlyDuration, addedTime } = this.props;

        if (onlyDuration) {
            this.setState(getDuration(addedTime));
        } else {
            this.start();
        }
    };

    componentWillReceiveProps = (nextProps: { stop: boolean }) => {
        if (this.props.stop !== nextProps.stop && !nextProps.stop) {
            this.start();
        }
        if (!this.props.onlyDuration && nextProps.onlyDuration) {
            this.stop();
            this.setState(getDuration(nextProps.addedTime));
        }
    };

    componentWillUnmount = () => {
        this.stop();
    };

    start = () => {
        const { interval } = this.props;
        this.timer = setInterval(this.counting, interval);
    };

    stop = () => {
        clearInterval(this.timer);
    };

    counting = () => {
        const { dateInitial, dateFinal, addedTime } = this.props;

        this.setState(getDifferenceTime({ dateFinal, dateInitial, addedTime }));
    };

    render = () => {
        const { years, months, days, hours, minutes, seconds } = this.state;
        const { title } = this.props;

        return (
            <div className="mn-header-info-column">
                <div className="label">{title}</div>
                <div className="value">
                    <span>
                        {years > 0 && `${years} años, `}
                        {months > 0 && `${months} meses y`}
                        {days > 0 && `${days} días`}
                    </span>
                    {days > 0 && <br />}
                    <span>
                        {`${hours} horas, ${minutes} min. y ${seconds} seg.`}
                    </span>
                </div>
            </div>
        );
    };
}

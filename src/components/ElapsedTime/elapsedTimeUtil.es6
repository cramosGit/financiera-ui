// @flow
import moment from "moment";

export const getDifferenceTime = (data: {
    dateFinal: number,
    dateInitial: number,
    addedTime: number
}) => {
    let { dateFinal, dateInitial, addedTime } = data;

    dateInitial = moment.unix(parseInt(dateInitial / 1000, 10));

    if (typeof dateFinal !== "undefined") {
        dateFinal = moment.unix(parseInt(dateFinal / 1000, 10));
    } else {
        dateFinal = moment();
    }

    if (addedTime > 0) {
        dateFinal = dateFinal.add(addedTime, "milliseconds");
    }

    const diff = moment.duration(dateFinal.diff(dateInitial)),
        years = diff.years(),
        months = diff.months(),
        days = diff.days(),
        hours = diff.hours(),
        minutes = diff.minutes(),
        seconds = diff.seconds();

    return {
        years,
        months,
        days,
        hours,
        minutes,
        seconds
    };
};

export const getDuration = (milliseconds: number) => {
    const tempTime = moment.duration(milliseconds, "milliseconds");

    const years = tempTime.years();
    const months = tempTime.months();
    const days = tempTime.days();
    const hours = tempTime.hours();
    const minutes = tempTime.minutes();
    const seconds = tempTime.seconds();

    return {
        years,
        months,
        days,
        hours,
        minutes,
        seconds
    };
};
